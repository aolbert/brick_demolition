class Bonus(Widget):
    velocity_x = NumericProperty(0)
    velocity_y = NumericProperty(-1/4)
    velocity = ReferenceListProperty(velocity_x, velocity_y)
    bonus_number = NumericProperty(0)

    def move(self):
        self.pos = Vector(*self.velocity) + self.pos

    def bonus_shape(self):
        self.bonus_number = random.randint(1,4)
        if self.bonus_number == 1:
            bonus_shape = 'wider.png'
        if self.bonus_number == 2:
            bonus_shape = 'super_ball.png'
        if self.bonus_number == 3:
            bonus_shape = 'more_balls.png'
        if self.bonus_number == 4:
            bonus_shape = 'life.png'
        return bonus_shape


def create_game(self):
        for i in range(1,11):
            for j in range(15):
                brick = Brick(pos = [self.width/15*j,self.height - self.height/30*i - self.height/6], size_hint = [None, None], size = [self.width/15, self.height/30] )
                a = random.randint(1,3)
                if(a!=3):
                    self.brick_container.add_widget(brick)
        self.paddle.center_x = self.width/2
        ball = Ball(pos = (self.paddle.center_x, self.paddle.height + 1), size_hint = [None,None], size = [self.width/50,self.width/50])
        self.ball_container.add_widget(ball)
        self.game_on_pause = True
        self.event = Clock.schedule_interval(self.update, 1/60)