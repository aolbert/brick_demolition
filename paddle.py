class Paddle(Widget):
    def bounce_ball(self,ball, testgame):
        vx, vy = ball.velocity
        if self.collide_widget(ball):
            # bounce of corners of the paddle
            if ((ball.center_y > self.y + self.height) and (ball.center_x > self.x + self.width)):
                ball.velocity = -vx, -vy
            elif ((ball.center_y > self.y + self.height) and (ball.center_x < self.x)):
                ball.velocity = -vx, -vy
            #bounce of the top of the paddle
            elif ball.center_y > self.y + self.height:
                a = ((ball.center_x - self.center_x)/ (self.width/2)) * ((2+3**(1/2))**(1/2))/2
                b = (1 - a**2) ** (1/2)
                ball.velocity = Vector(a*(1 + testgame.level*0.5),b*(1 + testgame.level*0.5))
            # bounce of the sides of the paddle
            elif ball.center_x > self.x + self.width or ball.center_x < self.x:
                ball.velocity = -vx,vy
    #wider paddle bonus
    def be_wider(self,TestGame):
        self.size = TestGame.width/3,TestGame.width/50

    def be_narrower(self,TestGame):
        self.size = TestGame.width/5,TestGame.width/50

def serve_ball(self):
    a = random.random()
    a_sign = random.choice([-1,1])
    b = (1 - a**2) ** (1/2)
    if(abs(b)/abs(a) < 2 - 3**1/2):
        a = ((2 + 3**(1/2))**(1/2))  / 2
    b = (1 - a**2) ** (1/2)
    self.ball_container.children[0].velocity = Vector(a_sign*a*(1 + self.level*0.5),b*(1 + self.level*0.5))