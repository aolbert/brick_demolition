class Ball(Widget):
    velocity_x = NumericProperty(0)
    velocity_y = NumericProperty(0)
    velocity = ReferenceListProperty(velocity_x,velocity_y)
    super_ball_property = BooleanProperty(False)

    def move(self):
        self.pos = Vector(*self.velocity) + self.pos




class Brick(Widget):
    def bounce_ball(self, ball):
        vx, vy = ball.velocity
        if self.collide_widget(ball) and ball.super_ball_property == False: 
            # bounce of corners of the brick
            if ((ball.center_y > self.y + self.height) and (ball.center_x > self.x + self.width)):
                ball.velocity = -vx, -vy
            elif ((ball.center_y > self.y + self.height) and (ball.center_x < self.x)):
                ball.velocity = -vx, -vy
            elif ((ball.center_y < self.y) and (ball.center_x > self.x + self.width)):
                ball.velocity = -vx, -vy
            elif ((ball.center_y < self.y) and (ball.center_x < self.x)):
                ball.velocity = -vx, -vy
            #bounce of the top and bottom of the brick
            elif ball.center_y > self.y + self.height or ball.center_y < self.y:
                ball.velocity = vx, -vy
            # bounce of the sides of the brick
            elif ball.center_x > self.x + self.width or ball.center_x < self.x:
                ball.velocity = -vx,vy
    def brick_colour(self):
        brick_color = random.choice(['brick.png','brick1.png','brick2.png','brick3.png','brick4.png','brick5.png','brick6.png','brick7.png','brick8.png','brick9.png'])
        return brick_color