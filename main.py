from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty,NumericProperty,ReferenceListProperty, ListProperty, BooleanProperty
from kivy.vector import Vector
from kivy.clock import Clock
from kivy.config import Config
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.screenmanager import ScreenManager, Screen
Config.set('graphics', 'resizable', '0') #0 being off 1 being on as in true/false
Config.set('graphics', 'width', '560')
Config.set('graphics', 'height', '960')
from kivy.core.window import Window
import random
from kivy.graphics import Rectangle

class Bonus(Widget):
    velocity_x = NumericProperty(0)
    velocity_y = NumericProperty(-1/4)
    velocity = ReferenceListProperty(velocity_x, velocity_y)
    bonus_number = NumericProperty(0)

    def move(self):
        self.pos = Vector(*self.velocity) + self.pos

    def bonus_shape(self):
        self.bonus_number = random.randint(1,4)
        if self.bonus_number == 1:
            bonus_shape = 'wider.png'
        if self.bonus_number == 2:
            bonus_shape = 'super_ball.png'
        if self.bonus_number == 3:
            bonus_shape = 'more_balls.png'
        if self.bonus_number == 4:
            bonus_shape = 'life.png'
        return bonus_shape
        


class Paddle(Widget):
    def bounce_ball(self,ball, testgame):
        vx, vy = ball.velocity
        if self.collide_widget(ball):
            # bounce of corners of the paddle
            if ((ball.center_y > self.y + self.height) and (ball.center_x > self.x + self.width)):
                ball.velocity = -vx, -vy
            elif ((ball.center_y > self.y + self.height) and (ball.center_x < self.x)):
                ball.velocity = -vx, -vy
            #bounce of the top of the paddle
            elif ball.center_y > self.y + self.height:
                a = ((ball.center_x - self.center_x)/ (self.width/2)) * ((2+3**(1/2))**(1/2))/2
                b = (1 - a**2) ** (1/2)
                ball.velocity = Vector(a*(1 + testgame.level*0.5),b*(1 + testgame.level*0.5))
            # bounce of the sides of the paddle
            elif ball.center_x > self.x + self.width or ball.center_x < self.x:
                ball.velocity = -vx,vy
    #wider paddle bonus
    def be_wider(self,TestGame):
        self.size = TestGame.width/3,TestGame.width/50

    def be_narrower(self,TestGame):
        self.size = TestGame.width/5,TestGame.width/50


class Ball(Widget):
    velocity_x = NumericProperty(0)
    velocity_y = NumericProperty(0)
    velocity = ReferenceListProperty(velocity_x,velocity_y)
    super_ball_property = BooleanProperty(False)

    def move(self):
        self.pos = Vector(*self.velocity) + self.pos




class Brick(Widget):
    def bounce_ball(self, ball):
        vx, vy = ball.velocity
        if self.collide_widget(ball) and ball.super_ball_property == False: 
            # bounce of corners of the brick
            if ((ball.center_y > self.y + self.height) and (ball.center_x > self.x + self.width)):
                ball.velocity = -vx, -vy
            elif ((ball.center_y > self.y + self.height) and (ball.center_x < self.x)):
                ball.velocity = -vx, -vy
            elif ((ball.center_y < self.y) and (ball.center_x > self.x + self.width)):
                ball.velocity = -vx, -vy
            elif ((ball.center_y < self.y) and (ball.center_x < self.x)):
                ball.velocity = -vx, -vy
            #bounce of the top and bottom of the brick
            elif ball.center_y > self.y + self.height or ball.center_y < self.y:
                ball.velocity = vx, -vy
            # bounce of the sides of the brick
            elif ball.center_x > self.x + self.width or ball.center_x < self.x:
                ball.velocity = -vx,vy
    def brick_colour(self):
        brick_color = random.choice(['brick.png','brick1.png','brick2.png','brick3.png','brick4.png','brick5.png','brick6.png','brick7.png','brick8.png','brick9.png'])
        return brick_color



class TestGame(Widget):
    #children widgets
    ball_container = ObjectProperty(None)
    brick_container = ObjectProperty(None)
    bonus_container = ObjectProperty(None)
    paddle = ObjectProperty(None)

    #game variables
    wider_paddle_duration = NumericProperty(0)
    super_ball_duration = NumericProperty(0)
    score = NumericProperty(0)
    level = NumericProperty(1)
    lifes = NumericProperty(2)
    mouse_position_x = NumericProperty(0)
    mouse_position_y = NumericProperty(0)
    game_on_pause = BooleanProperty(True)

    def create_game(self):
        for i in range(1,11):
            for j in range(15):
                brick = Brick(pos = [self.width/15*j,self.height - self.height/30*i - self.height/6], size_hint = [None, None], size = [self.width/15, self.height/30] )
                a = random.randint(1,3)
                if(a!=3):
                    self.brick_container.add_widget(brick)
        self.paddle.center_x = self.width/2
        ball = Ball(pos = (self.paddle.center_x, self.paddle.height + 1), size_hint = [None,None], size = [self.width/50,self.width/50])
        self.ball_container.add_widget(ball)
        self.game_on_pause = True
        self.event = Clock.schedule_interval(self.update, 1/60)


    def serve_ball(self):
        a = random.random()
        a_sign = random.choice([-1,1])
        b = (1 - a**2) ** (1/2)
        if(abs(b)/abs(a) < 2 - 3**1/2):
            a = ((2 + 3**(1/2))**(1/2))  / 2
        b = (1 - a**2) ** (1/2)
        self.ball_container.children[0].velocity = Vector(a_sign*a*(1 + self.level*0.5),b*(1 + self.level*0.5))
        
    #main loop of the game
    def update(self,*args):
        for i in range(5):

            #moving ball and bouncing it of the paddle
            for child in self.ball_container.children:
                self.paddle.bounce_ball(child, self)
                child.move()


            for brick_child in self.brick_container.children:
                for ball_child in self.ball_container.children:
                    brick_child.bounce_ball(ball_child)
                    if brick_child.collide_widget(ball_child):
                        a = random.randint(1,7)
                        if(a==1):
                            bonus = Bonus(pos = brick_child.center , size_hint = [None, None], size = [self.width/30, self.width/50])
                            self.bonus_container.add_widget(bonus)
                        self.brick_container.remove_widget(brick_child)
                        self.score = self.score + 1

            #bonuses
            for child in self.bonus_container.children:
                if child.collide_widget(self.paddle):
                    if(child.bonus_number == 1):
                        self.wider_paddle_duration = 1500
                    if(child.bonus_number == 2):
                        for ball_child in self.ball_container.children:
                            self.super_ball_duration = 1500
                    if(child.bonus_number == 3):
                        for ball_child in self.ball_container.children[:]:
                            ball = Ball(pos = (ball_child.x,ball_child.y),size_hint = [None, None], size = [self.width/50,self.width/50])
                            a = random.random()
                            a_sign = random.choice([-1,1])
                            b = (1 - a**2) ** (1/2)
                            b_sign = random.choice([-1,1])
                            if(abs(b)/abs(a) < 2 - 3**1/2):
                                a = ((2 + 3**(1/2))**(1/2))  / 2
                            b = (1 - a**2) ** (1/2)
                            ball.velocity = Vector(a_sign*a*(1 + self.level*0.5), b_sign*b*(1 + self.level*0.5))
                            self.ball_container.add_widget(ball)
                    if(child.bonus_number == 4):
                        self.lifes += 1
                    self.bonus_container.remove_widget(child)
                child.move()

            if(self.super_ball_duration == 1500):
                for child in self.ball_container.children:
                    child.super_ball_property = True

            if(self.super_ball_duration > 0):
                self.super_ball_duration -= 1
                if (self.super_ball_duration == 0):
                    for child in self.ball_container.children:
                        child.super_ball_property = False

            if(self.wider_paddle_duration == 1500):
                self.paddle.be_wider(self)

            if(self.wider_paddle_duration > 0):
                self.wider_paddle_duration -= 1
                if (self.wider_paddle_duration == 0):
                    self.paddle.be_narrower(self)

            

            for child in self.ball_container.children:
                #bounce off top
                if (child.top > self.height - self.height/6 - 1):
                    child.velocity_y *= -1

                #bounce off left and right
                if (child.x < 0) or (child.right > self.width):
                    child.velocity_x *= -1


            for child in self.ball_container.children[:]:
                if (child.y < 0):
                    self.ball_container.remove_widget(child)

            if (self.lifes == 0):
                self.event.cancel()
                self.parent.game_lost()
                break

            if (len(self.ball_container.children) == 0 and self.lifes > 0):
                self.lifes -= 1
                self.bonus_container.clear_widgets()
                self.ball_container.clear_widgets()
                self.paddle.center_x = self.width/2
                ball = Ball(pos = (self.paddle.center_x, self.paddle.height + 1), size_hint = [None,None], size = [self.width/50,self.width/50])
                self.ball_container.add_widget(ball)
                self.game_on_pause = True


            if(len(self.brick_container.children) == 0 and self.lifes > 0):
                self.level += 1
                self.brick_container.clear_widgets()
                self.ball_container.clear_widgets()
                self.bonus_container.clear_widgets()
                self.event.cancel()
                self.create_game()
                break
            
            

        
    
    def on_touch_down(self,touch):
        if(self.game_on_pause == True):
            self.mouse_position_x = touch.sx
            self.mouse_position_y = touch.sy
            self.game_on_pause = False
            self.serve_ball()


    def on_touch_move(self, touch):
        if touch.y < self.height / 3 and self.game_on_pause == False:
            self.paddle.center_x = touch.x


class WelcomeScreen(Screen):
    pass

class OptionsScreen(Screen):
    def function(self, my_color):
        Window.clearcolor = my_color

class PlaygroundScreen(Screen):
    def on_enter(self):
        self.game_engine.create_game()

    def game_lost(self):
        self.parent.clear_widgets()
        welcome = WelcomeScreen(name = "welcome_screen")
        playscreen = PlaygroundScreen(name = "playground_screen")
        options = OptionsScreen(name = "options_screen")
        self.parent.add_widget(welcome)
        self.parent.add_widget(options)
        self.parent.add_widget(playscreen)
        self.parent.current = "welcome_screen"

class BrickApp(App):
    def build(self):
        BrickApp.screen_manager = ScreenManager()
        welcome = WelcomeScreen(name = "welcome_screen")
        playscreen = PlaygroundScreen(name = "playground_screen")
        options = OptionsScreen(name = "options_screen")
        self.screen_manager.add_widget(welcome)
        self.screen_manager.add_widget(options)
        self.screen_manager.add_widget(playscreen)
        return self.screen_manager


if __name__ == '__main__':
    BrickApp().run()